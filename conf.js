'use strict';
module.exports = {
	'name': 'Hubzero Labs',
	'http': {
		'port': 8444
	},
	'pg': {
		'host': '127.0.0.1',
		'user': 'labs',
		'database': 'labs'
	}
};
