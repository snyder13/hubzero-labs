'use strict';
module.exports = ({ app }) => {
	app.use(app.hasDOM((req, res, next, $) => {
		res.renderer = new Promise((resolve) => {
			let body = res.lab.window.$('html')[0].outerHTML;
			if (res.lab.headers['content-type'] == 'text/x-partial-html') {
				// jsdom coerces fragments to full documents, this usually gets the fragment back (may need more finesse going forward)
				body = body.replace(/^.*?<body>|<\/body>.*?$/g, '');
				// done with this partial distinction, respond as html
				res.lab.headers['content-type'] = 'text/html';
			}
			resolve({
				'type': res.lab.headers['content-type'],
				'body': res.lab.window.$('html')[0].outerHTML
			});
		});
		next();
	}));
};
