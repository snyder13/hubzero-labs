'use strict';
// @flow

module.exports = ({ app, conf, log }) => {
	require('fs').readdirSync(`${__dirname}/../controllers/`).forEach((file) => {
		if (/[.]js$/.test(file)) {
			log.info(`\t\t${file}`);
			require(`${__dirname}/../controllers/${file}`)(app, conf, log);	
		}
	});
};
