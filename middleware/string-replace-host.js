'use strict';
// @flow
//
const escapeRe = require('escape-string-regexp');

module.exports = ({ app }) => {
	app.use(app.hasLabResponse((req, res, next) => {
		if (/html/.test(res.lab.headers['content-type'])) {
			// sometimes applications generate absolute URLs for themselves that turn out to be wrong,
			// because we want the hostname that aliases to the hubpub proxy and not the hostname of the actual hosting container
			//
			// this fixes most instances that crop up -- be careful though of links in outgoing emails, which probably do have to be fixed in the app code
			const hostRe = new RegExp('(https?:\/\/)?' + escapeRe(req.lab.forward.hostname) + '(:' + req.lab.forward.port + ')?', 'gi');
			res.lab.body = res.lab.body.toString().replace(hostRe, (_all, hasScheme) => {
				return (hasScheme ? 'https://' : '') + req.headers['x-forwarded-host'];
			});
		}
		next();
	}));
};
