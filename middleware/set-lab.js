'use strict';
// @flow
const url = require('url');

module.exports = ({ app, log }) => {
	// helper for middleware that wants only requests that are not served directly by the proxy (the stuff in /hubpub/...)
	app.unrouted = (fun) => {
		return (req, res, next) => {
			/^\/hubpub\//.test(req.path) ? next() : fun(req, res, next);
		};
	};
	// helper for middleware that wants only requests that have been matched up with a lab in the database
	app.hasLab = (fun) => {
		return (req, res, next) => {
			req.lab ? fun(req, res, next) : next();
		};
	};

	// There is a convention where the first part of a domain is used as the lab name, so if you had:
	//
	// myhub.org - CMS server
	//
	// labs.myhub.org - proxy server
	//
	// and two labs
	//
	// lab1.aws.myhub.org - container for application called "Foo"
	// lab2.aws.myhub.org - container for application called "Bar"
	//
	// You would then want to make two DNS aliases for the proxy server (or use a wildcard prefix):
	//
	// foo.labs.myhub.org -> labs.myhub.org
	// bar.labs.myhub.org -> labs.myhub.org
	//
	// The linkage between foo -> lab1 and bar -> lab2 is then made in the `labs` table, and the rest of the tables that key on that define behaviors of the proxy for that app

	// So, in short, the first step is to get that lab alias out of the hostname.
	app.use((req, res, next) => {
		/// @DEBUG
		// Apache or whatever is proxying for us isn't setting the header, and we just think of ourselves as labs.myhub.org without further direction. Change that software's conf to set this
		if (!req.headers['x-forwarded-host']) {
			return res.status(500).render('errors/generic', { 'title': 'Internal server error', 'detail': 'Forward proxy did not set x-forwarded-host. This is a misconfiguration.' });
		}
		req.labName = req.headers['x-forwarded-host'].replace(/[.].+$/, '');
		return next();
	});

	app.use(app.unrouted((req, res, next) => {
		//Select configuration for the lab -- you can see more about each of these data in the middleware associated with them
		app.pg.query(
			`SELECT
				labs.*,
				(SELECT array_to_json(array_agg(json_build_array(pattern, redirect)))
					FROM url_rejections
					WHERE lab_id = labs.id) AS url_rejections,
				(SELECT array_to_json(array_agg(host))
					FROM allowed_links
					WHERE lab_id = labs.id) AS allowed_hosts,
				(SELECT array_to_json(array_agg(selector))
					FROM dom_rejections
					WHERE lab_id = labs.id) AS dom_rejections
			FROM labs
			WHERE name = $1`, [ req.labName ])
		.then((labDef) => {
			if (labDef.rows.length == 0) {
				return next();
			}
			req.lab = labDef.rows[0];
			req.lab.forward = url.parse(req.lab.forward);
			next();
		}, (err) => {
			log.error('database error', { 'message': err.toString() });
			res.status(500).render('errors/generic', { 'title': 'Database Error' });
		});
	}));
};
