'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const
	http = require('http'),
	https = require('https'),
	log = global.logger
	;


module.exports = ({ app, conf, log }) => {
	// helper for other middleware that only wants to be called for lab responses
	app.hasLabResponse = (fun) => {
		return (req, res, next) => {
			res.lab && !/^\/hubpub\//.test(req.path) ? fun(req, res, next) : next();
		};
	};

	app.use(app.hasLab((req, res, next) => {
		const dbg = req.method === 'POST';
		if (req.rawBody && req.method === 'POST') {
			req.fwdHeaders['content-length'] = req.rawBody.length;
		}
		// forward request to the lab server
		let appSrv = (req.lab.forward.protocol == 'http:' ? http : https).request({
			'method': req.method,
			'hostname': req.lab.forward.hostname,
			'path': req.url,
			'port': req.lab.forward.port,
			'headers': req.fwdHeaders
		}, (appRes) => {
			let payload = [];
			appRes.on('data', (chunk) => {
				payload.push(chunk);
			});
			appRes.on('end', (err) => {
				res.lab = {
					'status': appRes.statusCode,
					'body': Buffer.concat(payload), // nb: Buffer.concat instead of [].join to avoid misinterpreting utf8 characters that span chunk boundaries
					'headers': appRes.headers
				};
				res.renderer = new Promise((resolve) => {
					if (!res.headersSent) {
						Object.keys(res.lab.headers).forEach((k) => {
							res.set(k, res.lab.headers[k]);
						});
					}
					resolve({
						'type': res.lab.headers['content-type'],
						'body': res.lab.body
					});
				});
				next();
			});
		});

		appSrv.on('socket', (sock) => {
			sock.setTimeout(req.lab.timeout_seconds * 1000, () => {
				res.status(503).render('errors/generic', { 'title': 'Service Unavailable', 'detail': 'The laboratory ran out of time to respond. It may be temporarily overburdened or misconfigured. Try again later' });
			})
		});

		appSrv.on('error', (ex) => {
			log.error(`error forwarding to ${req.lab.name} using ${JSON.stringify(req.lab.forward)}`, ex);
			res.status(503).render('errors/generic', { 'title': 'Service Unavailable', 'detail': 'Failed to connect to the laboratory. It may be down for maintenance or misconfigured. Try again later' });
		});
		appSrv.write(req.rawBody ? req.rawBody : '');
		appSrv.end();
	}));
};
