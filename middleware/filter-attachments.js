'use strict';

module.exports = ({ app }) => {
	app.use(app.hasLabResponse((req, res, next) => {
		if (res.lab.headers['location']) {
			res.redirect(res.lab.headers['location']);
			return next();
		}
		if (/html/.test(res.lab.headers['content-type'])) {
			return next();
		}

		/**
		 * @@TODO this should actually do some filtering, but it's complicated given some of the applications we have that legitimately want to send users, e.g., application/octet-stream
		 */

		res.renderer = new Promise((resolve) => {
			resolve({
				'type': res.lab.headers['content-type'],
				'body': res.lab.body
			});
		});
		next();
	}));
};
