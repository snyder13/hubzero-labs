'use strict';
module.exports = ({ app, conf }) => {
	// some headers create problems, cookies are just handled specially by Express
	// here we clear some headers by setting them null or force specific values for others
	// this list can be augmented in conf.js -> 'force-response-headers': { ... }
	const forceRespHeaders = conf.get('force-response-headers', {});
	forceRespHeaders['x-powered-by'] = null;
	forceRespHeaders['x-frame-options'] = null;
//	forceRespHeaders['set-cookie'] = null;
	forceRespHeaders['connection'] = 'close';
	forceRespHeaders['server'] = null;

	app.use(app.hasLabResponse((req, res, next) => {
		if (res.lab.headers.location) {
			res.lab.headers.location = res.lab.headers.location
				.replace(/^(.*?)\/\/[^\/]+/, `https://${req.headers['x-forwarded-host']}`);
		}
		if (res.lab.headers['set-cookie'] && res.lab.headers['set-cookie'].length) {
			/// not sure if multiple cookies in a response ever works -- it's discouraged but not disallowed by the spec. if the forwarded app tries it just try the same
			res.lab.headers['set-cookie'].forEach((ck) => {
				res.setHeader('set-cookie', ck);
			});
		}

		Object.keys(forceRespHeaders).forEach((k) => {
			if (forceRespHeaders[k] === null) {
				delete res.lab.headers[k];
			}
			else {
				res.lab.headers[k] = forceRespHeaders[k];
			}
		});
		next();
	}));
};
