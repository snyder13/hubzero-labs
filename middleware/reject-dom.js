'use strict';

module.exports = ({ app }) => {
	app.use(app.hasDOM((req, res, next, $) => {
		/**
		 * This middleware is intended to remove elements from the responses on-the-wire so that they are still usable in standalone mode
		 *
		 * One common example is a login/logout area which might still be useful in standalone mode but doesn't look or behave right when the lab is using the hub's session
		 */
		if (req.lab.dom_rejections) {
			req.lab.dom_rejections.some((rej) => {
				$(rej).remove();
			});
		}
		next();
	}));
};
