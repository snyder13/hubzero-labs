'use strict';
// @flow

module.exports = ({ app }) => {
	app.use(app.hasLab((req, res, next) => {
		if (!req.lab.url_rejections) {
			return next();
		}
		/**
		 * This is for refusing service to certain areas of the site, like perhaps if it has an /admin area that you only want the devs to be able to use in standalone mode
		 *
		 * If the rejection is defined to have a redirect in the database it'll do that, otherwise it will just clear the lab response so no middleware involving it is called.
		 *
		 * Typically that means it lands on a 404 page, but other middleware still has a chance to do something about it.
		 */
		req.lab.url_rejections.some((rej) => {
			if (new RegExp(rej[0]).test(req.path)) {
				delete req.lab;
				if (rej[1]) {
					res.redirect(rej[1]);
				}
				return true;
			}
		});
		next();
	}));
};
