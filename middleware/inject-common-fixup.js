'use strict';

const fs = require('fs');

module.exports = ({ app, conf }) => {
	const
		pathCache = {},
		staticRoot = conf.get('static-root', ''),
	// check whether the lab has a custom CSS file defined on the proxy
	// @TODO requires server restart to add one, should probably have expiry
	getLabCssPath = (lab) => {
		if (pathCache[lab] === undefined) {
			const path = `${global.VEXIO_ROOT}/app/public/css/patches/${lab}.css`;
			pathCache[lab] = fs.existsSync(path) ? `${staticRoot}/css/patches/${lab}.css` : null;
		}
		return pathCache[lab];
	};

	app.use(app.hasDOM((req, res, next, $) => {
		// for labs that return html fragments on xhr, they can set their content type as text/x-partial-html to prevent it from being augmented here (the content type is changed back to text/html elsewhere before sending)
		if (/partial/.test(res.lab.headers['content-type'])) {
			return next();
		}

		/// @DEBUG
		const
			head = $('head').prepend($('<script>').attr('src', '/js/lib/iframeResizer.contentWindow.js')),
			cssPath = getLabCssPath(req.lab.name)
			;
		if (cssPath) {
			head.prepend($('<link rel="stylesheet" />').attr('href', cssPath));
		}
		next();
	}));
};
