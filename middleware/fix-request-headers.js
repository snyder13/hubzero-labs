'use strict';
// @flow	
const crypto = require('crypto');

module.exports = ({ app, conf }) => {
	let forceReqHeaders = conf.get('force-request-headers', {});
	forceReqHeaders['connection'] = 'close';
	forceReqHeaders['accept-encoding'] = null;
	forceReqHeaders['x-forwarded-host'] = null;

	app.use(app.hasLab((req, res, next) => {
		// forward incoming headers
		req.fwdHeaders = {}
		Object.keys(req.headers).forEach((k) => {
			if (forceReqHeaders[k] === null) {
				return;
			}
			req.fwdHeaders[k] = req.headers[k];
		});
		Object.keys(forceReqHeaders).forEach((k) => {
			if (forceReqHeaders[k] === null) {
				delete req.fwdHeaders[k];
			}
			else {
				req.fwdHeaders[k] = forceReqHeaders[k];
			}
		});

		// change client-facing hostname that is actually this proxy to the actual hostname of the lab machine so virtual hosting works there
		req.fwdHeaders.host = req.lab.forward.hostname;

		// append session information
		const
			nonce = crypto.randomBytes(conf.get('nonce-size', 40)).toString('base64'),
			jsonAttrs = typeof(req.session.attrs) === 'string' ? req.session.attrs : JSON.stringify(req.session.attrs)
			;

		if (req.session && req.session.attrs) {
			// make a signature for the session attributes with a nonce and the shared key for this lab
			// the lab should in turn verify in the headers that sha(session + nonce + shared key) === hash or discard the session
			let sha1 = crypto.createHash('sha1');
			sha1.update(jsonAttrs, 'utf8');
			sha1.update(nonce, 'utf8');
			sha1.update(req.lab.shared_key, 'utf8');
			req.fwdHeaders['x-hubpub-session'] = jsonAttrs;
			req.fwdHeaders['x-hubpub-nonce'] = nonce;
			req.fwdHeaders['x-hubpub-hash'] = sha1.digest('hex');
			const attrs = JSON.parse(jsonAttrs);
			for (const k in attrs) {
				req.fwdHeaders[`x-hubpub-${k}`] = attrs[k];
			}
		}
		next();
	}));
};
