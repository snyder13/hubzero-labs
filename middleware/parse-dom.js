'use strict';

let
	jsdom = require('jsdom/lib/old-api'),
	jQuerySrc = require('fs').readFileSync(`${__dirname}/../public/js/lib/jquery.min.js`)
	;

module.exports = ({ app, log }) => {
	/**
	 * helper for middleware that only wants to deal with parsed HTML documents
	 *
	 * passes the document's jQuery instance as the 4th param to these middlewarer
	 */
	app.hasDOM = (fun) => {
		return (req, res, next) => {
			res.lab && res.lab.window ? fun(req, res, next, res.lab.window.$) : next();
		};
	};
	app.use(app.hasLabResponse((req, res, next) => {
		if (/html/.test(res.lab.headers['content-type']) && !res.lab.headers['location'] && !/[.]php$/.test(req.path)) {
			jsdom.env({
				'html': res.lab.body,
				'src': [ jQuerySrc ],
				'done': (err, window) => {
					// this is really hard to trigger -- for better or worse jsdom will go through extraordinary lengths to reconcile malformed html
					if (err) {
						log.error('error interpreting html', { err });
						return res.status(503).render('errors/generic', { 'title': 'Service unavailable', 'detail': 'Failed to interpret lab HTML response.' });
					}
					res.lab.window = window;
					next();
				}
			});
		}
		else {
			next();
		}
	}));
};
