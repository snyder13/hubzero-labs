'use strict';

const url = require('url'), crypto = require('crypto');

module.exports = ({ app, conf }) => {
	app.use(app.hasDOM((req, res, next, $) => {
		const nonce = crypto.randomBytes(conf.get('nonce-size', 32)).toString('base64').replace(/=+$/, '');
		$('a').each((_, el) => {
			el = $(el);
			try {
				const
					rawHref = el.attr('href'),
					href = url.parse(rawHref)
					;
				if (href.host && href.host !== req.headers['x-forwarded-host']) {
					// allowed links go through a Facebook-style "you're leaving the site" interstitial
					if (req.lab.allowed_hosts['*'] || (req.lab.allowed_hosts && req.lab.allowed_hosts.indexOf(href.host) > -1)) {
						// hash the nonce with the lab key so the `leave` controller can verify it.
						// this mitigates the possibility of people changing the to= param to get a favorable bounce from this page, which could let them use links in phishing mails that are actually on the hub domain
						let sha1 = crypto.createHash('sha1');
						sha1.update(rawHref, 'utf8');
						sha1.update(nonce, 'utf8');
						sha1.update(req.lab.shared_key, 'utf8');

						el.attr('href', '/hubpub/leave?to=' + encodeURIComponent(rawHref) + '&nonce=' + encodeURIComponent(nonce) + '&hash=' + sha1.digest('hex'));
					}
					else {
						el.removeAttr('href');
					}
				}
			}
			catch (_ex) {
				// badly malformed URL - no link for you
				el.removeAttr('href');
			}
		});
		next();
	}));
};
