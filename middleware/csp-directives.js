'use strict';

module.exports = ({ app, conf }) => {
	app.use(app.hasDOM((req, res, next) => {
		/**
		 * The table stores directive (k,v) pairs for each lab that has specific definitions, along with defaults for all labs when the lab_id is null.
		 *
		 * The first part of this union gets the default set and overrides them wherever there is a specific entry.
		 *
		 * The second is there to support the case where there is a specific key for a lab that does not exist in the defaults. (So a key can default to not existing at all)
		 */
		app.pg.query(
			`SELECT def.key, coalesce(lab.value, def.value) AS value
				FROM csp_directives def
				LEFT JOIN csp_directives lab ON lab.key = def.key AND lab.lab_id = $1
				WHERE def.lab_id IS NULL
			UNION
			SELECT key, value
				FROM csp_directives
				WHERE lab_id = $1
					AND key NOT IN (
						SELECT DISTINCT key
						FROM csp_directives
						WHERE lab_id IS NULL
					)
			`, [ req.lab.id ])
		.then((csp) => {
			const header = csp.rows
				// don't include keys with blank values
				.filter((row) => {
					return row.value !== null && row.value.replace(/^\s+|\s+$/, '') !== '';
				})
				// replace the $HUB_HOST variable, which allows labs to share CSS and other media with the hub when present
				.map((row) => {
					return row.key + ' ' + row.value.replace(/\$HUB_HOST/g, req.lab.hub_host + ' ' + req.lab.hub_host.replace('//', '//*.'));
				})
				.join('; ');

			if (header) {
				res.set(req.lab.csp_enabled ? 'content-security-policy' : 'content-security-policy-report-only', header);
			}
			next();
		}, (err) => {
			log.error('database error', { 'message': err.toString() });
			res.status(500).render('errors/generic', { 'title': 'Database Error' });
		});
	}));
};

