'use strict';
let express = require('express'), app = express(),
	crypto = require('crypto'),
	fs = require('fs'),
	cookieParser = require('cookie-parser')
	;

let testKey = 'testsharedkeyshouldactuallyberandom';

app.use(express.static('incoming'));
app.use(cookieParser());
app.use(require('express-fileupload')());

app.get('*', (req, res) => {
	let hash, testHash, session;
	if (req.headers['x-hubpub-session'] && req.headers['x-hubpub-nonce'] && req.headers['x-hubpub-hash']) {
		let sha1 = crypto.createHash('sha1');
		sha1.update(req.headers['x-hubpub-session'], 'utf8');
		sha1.update(req.headers['x-hubpub-nonce'], 'utf8');
		sha1.update(testKey, 'utf8');
		testHash = sha1.digest('hex');
		hash = req.headers['x-hubpub-hash'];
	}

	res.cookie('somecookie', 42, { 'maxAge': 900, 'httpOnly': true });
	res.send(
`<!doctype html>
<html>
<head>
	<title>Test Lab</title>
</head>
<body>
<div class="reject">Remove me</div>
<h1><a href="https://${req.headers.host}${req.path}">${req.headers.host}${req.path}</a></h1>
<h2>Session:</h2>
<pre>${hash} vs. ${testHash}</pre>
<pre>${session}</pre>
<h3>Cookie:</h3>
<pre>${JSON.stringify(req.cookies)}</pre>
<h2>Content security:</h2>
<ul>
	<li><a href="https://google.com">Allowed off-site link</a></li>
	<li><a href="https://yahoo.com">Disallowed off-site link</a></li>
	<li><a href="/other/path">Relative link</a></li>
</ul>
<form action="/upload" method="post" enctype="multipart/form-data">
	<input type="file" name="file">
	<input type="hidden" name="other-mp-field" value="test">
	<input type="submit">
</form>
<img src="/upload.png">
</body>
</html>
`
	);
});

app.post('*', (req, res) => {
	res.cookie('somecookie', 42, { 'maxAge': 900000, 'httpOnly': true });
	if (req.files && req.files.file) {
		console.log('mv', __dirname + '/incoming/upload.png');
		req.files.file.mv(__dirname + '/incoming/upload.png', (err) => {
			if (err) {
				res.status(500).send(err);
			}
			else {
				res.redirect('/');
			}
		});
	}
	else {
		res.status(400).send('bad request');
	}
});

app.listen(3872, () => {
  console.log('test app listening :3872');
});
