'use strict';
let crypto = require('crypto');

let hasKeys = (cont) => {
	return (req, res, keys) => {
		if (!req[cont] || keys.some((key) => { return !req[cont][key]; })) {
			res.status(400).render('errors/generic', { 'title': 'Bad request', 'detail': 'The request could not be completed because not all of the required parameters were submitted.' });
			return false;
		}
		return true;
	}
};

let fail = (res) => {
	res.status(403).render('errors/generic', { 'title': 'Forbidden', 'detail': 'The request could not be completed because its sender could not be authenticated.' });
	return false;
};

let hash = (expected, data, res) => {
	let sha1 = crypto.createHash('sha1');
	data.forEach((v) => {
		sha1.update(v, 'utf8');
	});
	if (sha1.digest('hex') !== expected) {
		return fail(res);
	}
	return true;
};

module.exports = {
	'assertInternal': (req, res, conf) => {
		if (req.headers['x-hubpub-shared-key']) {
			if (req.headers['x-hubpub-shared-key'] !== conf.get('shared-key')) {
				fail(res);
			}
			return true;
		}
		if (!req.body || !req.headers['x-hubpub-hash'] || !req.headers['x-hubpub-nonce']) {
			console.log({ 'body': req.body, 'headers': req.headers });
			return fail(res);
		}
		return hash(req.headers['x-hubpub-hash'], [
			JSON.stringify(req.body).replace(/\//g, '\\/'), // PHP json-encoding escapes forward slashes, node's does not. make them equivalent
			req.headers['x-hubpub-nonce'],
			conf.get('shared-key')
		], res);
	},
	'hasBodyKeys': hasKeys('body'),
	'hasQueryKeys': hasKeys('query'),
	'hash': hash
};
