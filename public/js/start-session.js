jQuery(function() {
	if (window.executedInline) {
		$('.throbber').hide();
		$('.warning').fadeIn();
	}
	else {
		$('.continue').click();
	}
});
