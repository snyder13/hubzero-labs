module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'hubzero-labs',
      script    : 'server.js',
	'env': {
		'DEBUG': 'hex:*',
		'NODE_PORT': '8452'
	}
    }
  ]
};
