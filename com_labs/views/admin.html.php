<style type="text/css">
.info {
	display: inline-block;
}
.text label, .label {
	display: inline-block;
	width: 160px;
	text-align: right;
	padding-right: 10px;
}
.checkbox {
	margin-left: 170px;
}
.inp {
	display: inline-block;
}
.inp-cont {
	position: relative;
}
.inp-cont .info {
	position: relative;
	top: 16px;
	left: 16px;
}
input[type=text] {
	width: 400px;
}
.attrs ul {
	list-style: none;
	margin: 0;
}
p {
	margin: 0;
}
.labs {
	list-style: none;
}
.labs > li {
	padding: 2em;
	border: 2px solid #ddd;
	border-radius: 16px;
	margin-bottom: 1em;
}
#app {
	margin: 2em;
}
.confirmation {
	margin: 10px;
	color: #090;
	background: #efe;
	padding: 5px;
	border-radius: 5px;
	display: inline-block;
}
tt {
	color: #336;
}
</style>
<main id="app">
</main>
<?php $dist = str_replace(JPATH_BASE, '', preg_replace('#/site$#', '', PATH_COMPONENT)) . '/admin/dist'; ?>
<script src="<?php echo $dist; ?>/vendor/polyfill.min.js"></script>
<script src="<?php echo $dist; ?>/bundle.js"></script>
