<?php namespace Hubzero\Labs;

defined('JPATH_BASE') or die();

class HttpError extends \Exception
{
	private $httpCode;
	public function __construct($httpCode, $msg) {
		$this->httpCode = $httpCode;
		parent::__construct($msg);
	}
	public function getHttpCode() { return $this->httpCode; }
}

class BadRequestError extends HttpError
{
	public function __construct($msg = 'Bad Request') {
		parent::__construct(400, $msg);
	}
}

class NotFoundError extends HttpError
{
	public function __construct($msg = 'Not Found') {
		parent::__construct(404, $msg);
	}
}

class InternalServerError extends HttpError
{
	public function __construct($msg = 'Internal Server Error') {
		parent::__construct(500, $msg);
	}
}

class ServiceUnavailableError extends HttpError
{
	public function __construct($msg = 'Service Unavailable') {
		parent::__construct(503, $msg);
	}
}

class ForbiddenError extends HttpError
{
	public function __construct($msg = 'Forbidden') {
		parent::__construct(403, $msg);
	}
}

class DBO extends \PDO {
	private $prefix;

	public function __construct() {
		$conf = require JPATH_BASE.'/app/config/database.php';
		try {
			parent::__construct('mysql:host='.$conf['host'].';dbname='.$conf['db'], $conf['user'], $conf['password']);
			$attrs = [
				\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
				\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
				\PDO::ATTR_AUTOCOMMIT         => false
			];
			array_walk($attrs, function($v, $k) {
				$this->setAttribute($k, $v);
			});
			$this->prefix = $conf['dbprefix'];
		}
		catch (\Exception $ex) {
			throw new InternalServerError('Failed to establish database connection');
		}
	}

	public function prepare($sql, $opts = []) {
		return parent::prepare(str_replace('#__', $this->prefix, $sql), $opts);
	}
}

class Controller {
	private $resp, $dbh, $user, $profile;
	private static $codeMessages = [
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Payload Too Large',
		414 => 'URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Range Not Satisfiable',
		417 => 'Expectation Failed',
		418 => 'I\'m a teapot',
		421 => 'Misdirected Request',
		422 => 'Unprocessable Entity',
		423 => 'Locked',
		424 => 'Failed Dependency',
		426 => 'Upgrade Required',
		428 => 'Precondition Required',
		429 => 'Too Many Requests',
		431 => 'Request Header Fields Too Large',
		451 => 'Unavailable For Legal Reasons',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported',
		506 => 'Variant Also Negotiates',
		507 => 'Insufficient Storage',
		508 => 'Loop Detected',
		510 => 'Not Extended',
		511 => 'Network Authentication Required'
	];

	public function __construct() {
		$this->user = \JFactory::getUser();
		\JFactory::getDocument()->setTitle('Labs');
		$tasks = array_flip([
			'index', 'run', 'meta', 'admin', 'command'
		]);
		$task = 'index';
		$urlRest = '';
		if (preg_match('#^/labs/([-_a-z]+)(?:/(.*?))?$#', $_SERVER[isset($_SERVER['REDIRECT_SCRIPT_URL']) ? 'REDIRECT_SCRIPT_URL' : 'SCRIPT_URL'], $ma)) {
			if (!isset($tasks[$ma[1]])) {
				throw new NotFoundError;
			}
			$task = $ma[1];
			if (isset($ma[2])) {
				$urlRest = $ma[2];
			}
		}
		$this->dbh = new DBO;
		$this->$task(explode('/', $urlRest));
	}

	public function __destruct() {
		if ($this->resp[0] !== 'text/html') {
			exit();
		}
	}

	public function index() {
		header('Location: /resources/tools');
		exit();
	}

	public function assertAdmin() {
		if ($this->user->usertype !== 'Super Administrator') {
			throw new ForbiddenError('Access is limited to site administrators');
		}
	}

	public function saveLab($lab) {
		$err = function($msg) use($lab) {
			$this->resp = [ 'application/json', [ 'error' => $msg, 'ctx' => [ 'id' => $lab['id' ]] ], 400 ];
		};
		$missing = [];
		foreach ([ 'forward', 'host', 'id', 'name', 'title', 'shared_key', 'published' ] as $reqd) {
			if (!$lab[$reqd]) {
				$missing[] = $reqd;
			}
		}
		if ($missing) {
			return $err('Missing required field' . (count($missing) === 1 ? ': ' : 's: ') . implode(', ', $missing));
		}

		$isNew = preg_match('/^new/', $lab['id']);
		if ($isNew) {
			// names are also unique b/c they become the URL identifier for a lab. perhaps they should have never had numeric ids as well
			$sth = $this->dbh->prepare('SELECT 1 FROM #__labs WHERE name = :name');
			$sth->execute([ 'name' => $lab['name'] ]);
			if ($sth->fetch(\PDO::FETCH_NUM)) {
				return $err('Duplicate lab name');
			}
		}
		else {
			$sth = $this->dbh->prepare('SELECT name FROM #__labs WHERE id = :id');
			$sth->execute([ 'id' => $lab['id'] ]);
			list($oldName) = $sth->fetch(\PDO::FETCH_NUM);
			if (!$oldName) {
				return $err('Cannot update by non-existent id');
			}
			// if the name needs to be changed, do so first, since (again) the name is the URL identifier
			if ($oldName !== $lab['name']) {
				self::callProxy($lab['shared_key'], 'POST', $lab['host'].'/hubpub/rename/'.urlencode($oldName).'/'.urlencode($lab['name']));
			}
		}
		// proxy doesn't need a lot of metadata, just what host we are (for link sanitization) and where to forward requests
		self::callProxy($lab['shared_key'], 'POST', $lab['host'].'/hubpub/update/'.urlencode($lab['name']), [
			'hub_host' => 'https://'.preg_replace('/^dev[.]/', '', $_SERVER['HTTP_HOST']),
			'forward' => $lab['forward']
		]);

		$group = null;
		// look up the group if applicable, can either be provided by numeric id or common name
		if ($lab['gcn']) {
			$sth = $this->dbh->prepare('SELECT gidNumber FROM #__xgroups WHERE cn = :x OR gidNumber = :x LIMIT 1');
			$sth->execute([ ':x' => $lab['gcn'] ]);
			list($group) = $sth->fetch(\PDO::FETCH_NUM);
			if (!$group) {
				return $err('Invalid group common name or id number');
			}
		}

		$id = $lab['id'];
		if ($isNew) {
			$sth = $this->dbh->prepare('INSERT INTO #__labs (name, title, published, access_group) VALUES (:name, :title, :published, :group)');
			$sth->execute([
				'name' => $lab['name'],
				'title' => $lab['title'],
				'published' => !!$lab['published'],
				'group' => $group
			]);
			$sth = $this->dbh->prepare('SELECT id FROM #__labs WHERE name = :name');
			$sth->execute([ 'name' => $lab['name'] ]);
			$id = $sth->fetch(\PDO::FETCH_NUM);

			$sth = $this->dbh->prepare('INSERT INTO #__labs_proxy(published, host, shared_key, forward, lab_id) VALUES (:published, :host, :shared_key, :forward, :lab_id)');
			$sth->execute([
				'published' => !!$lab['published'],
				'host' => $lab['host'],
				'shared_key' => $lab['shared_key'],
				'forward' => $lab['forward'],
				'lab_id' => $id
			]);
		}
		else {
			$sth = $this->dbh->prepare('UPDATE #__labs SET name = :name, title = :title, published = :published, access_group = :group WHERE id = :id');
			$sth->execute([
				'name' => $lab['name'],
				'title' => $lab['title'],
				'published' => !!$lab['published'],
				'group' => $group,
				'id' => $lab['id']
			]);
			// technicall there is support for multiple proxies for one app but it's never come up and probably not worth ironing out the rest of the way
			$sth = $this->dbh->prepare('UPDATE #__labs_proxy SET published = :published, host = :host, shared_key = :shared_key, forward = :forward WHERE lab_id = :lab_id');
			$sth->execute([
				'published' => !!$lab['published'],
				'host' => $lab['host'],
				'shared_key' => $lab['shared_key'],
				'forward' => $lab['forward'],
				'lab_id' => $lab['id']
			]);
		}

		// clear and re-add any released profile fields
		$sth = $this->dbh->prepare('DELETE FROM #__lab_attribute_release WHERE lab_id = :lab_id');
		$sth->execute([ 'lab_id' => $id ]);
		$sth = $this->dbh->prepare('INSERT INTO #__lab_attribute_release (lab_id, attribute) VALUES (:lab_id, :attribute)');
		foreach ($lab as $key=>$val) {
			if ($val && preg_match('/^released_(.*)/', $key, $res)) {
				$sth->execute([
					'lab_id' => $id,
					'attribute' => $res[1]
				]);
			}
		}

		$this->resp = [ 'application/json', [ 'id' => $id, 'tempId' => $lab['id'], 'nameChange' => $doNameChange, 'args' => $lab ] ];
	}

	public function labsEndpoint() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			return $this->saveLab(json_decode(file_get_contents('php://input'), true));
		}
		$sth = $this->dbh->prepare(
			'SELECT
				l.id, l.name, l.title, l.published, g.cn AS gcn,
				lp.host, lp.forward, lp.shared_key, lp.forward,
				group_concat(ar.attribute) AS released
			FROM #__labs l
			INNER JOIN #__labs_proxy lp ON lp.lab_id = l.id
			LEFT JOIN #__xgroups g ON g.gidNumber = l.access_group
			LEFT JOIN #__lab_attribute_release ar ON ar.lab_id = l.id
			GROUP BY id, name, title, published, cn, host, forward, shared_key
			ORDER BY name'
		);
		$sth->execute();
		$this->resp = ['application/json', array_map(function($row) {
			foreach (explode(',', $row['released']) as $attr) {
				if ($attr) {
					$row['released_' . $attr] = true;
				}
			}
			unset($row['released']);
			return $row;
		}, $sth->fetchAll(\PDO::FETCH_ASSOC))];
	}

	public function sessionSecretEndpoint($lab) {
		$sth = $this->dbh->prepare('SELECT shared_key, host FROM #__labs_proxy lp INNER JOIN #__labs l ON l.id = lp.lab_id AND l.name = :name');
		$sth->execute([ ':name' => $lab ]);
		$row = $sth->fetch(\PDO::FETCH_ASSOC);
		if (!$row) {
			throw new NotFoundError('Not found');
		}
		$res = self::callProxy($row['shared_key'], 'GET', 'https://'.$row['host'].'/hubpub/session-secret/'.$lab);
		$res = json_decode($res, true);
		$res['name'] = $lab;
		$this->resp = [ 'application/json', $res ];
	}

	public function admin($rest) {
		$this->assertAdmin();
		if ($rest[0] === 'labs') {
			return $this->labsEndpoint();
		}
		if ($rest[0] === 'session-secret' && $rest[1]) {
			return $this->sessionSecretEndpoint($rest[1]);
		}
		$this->resp = ['text/html', self::view('admin')];
	}

	public function run($rest) {
		if (!$rest) {
			throw new NotFoundError;
		}
		if (isset($_GET['code']) && isset(self::$codeMessages[$_GET['code']])) {
			\JError::raiseError($_GET['code'], self::$codeMessages[$_GET['code']]);
		}
		$app = array_shift($rest);

		// get exported attribute keys
		$sth = $this->dbh->prepare(
			'SELECT l.id AS lab_id, title, login_scope, access_group, login_id, group_concat(ar.attribute separator \',\') AS attrs
			FROM #__labs l
			LEFT JOIN #__lab_attribute_release ar ON ar.lab_id = l.id
			WHERE l.published AND l.name = :name');
		$sth->execute([':name' => $app]);
		$attrList = $sth->fetch(\PDO::FETCH_ASSOC);

		// bail if not even a valid lab name
		if (!$attrList['lab_id']) {
			throw new NotFoundError;
		}

		if (!is_null($attrList['login_scope']) && $this->user->guest) {
			\JFactory::getApplication()->redirect('/login?return='.base64_encode(isset($_SERVER['REDIRECT_SCRIPT_URL']) ? $_SERVER['REDIRECT_SCRIPT_URL'] : $_SERVER['REDIRECT_URL']));
		}

		if (!is_null($attrList['access_group'])) {
			$this->assertGroup($attrList['access_group']);
		}

		// get a proxy for the lab request
		$sth = $this->dbh->prepare('SELECT host, forward, shared_key FROM #__labs_proxy WHERE published AND lab_id = :lab_id ORDER BY ordering LIMIT 1');
		$sth->execute([':lab_id' => $attrList['lab_id']]);
		$proxy = $sth->fetch();
		// no proxy no serve-y
		if (!$proxy) {
			throw new ServiceUnavailableError;
		}

		// resolve released attributes
		$attrs = [];
		foreach (explode(',', $attrList['attrs']) as $attr) {
			$attrs[$attr] = $this->attributeRelease($attr);
		}

		$cookieKey = sha1('hp-'.$app);
		if (isset($_COOKIE[$cookieKey])) {
			$sessKey = $_COOKIE[$cookieKey];
		}
		else {
			$sessKey = base64_encode(openssl_random_pseudo_bytes(32));
			///@FIXME
			setcookie($cookieKey, $sessKey, 0, '/', null, true, true);
		}

		// post attribute map to proxy
		$qs = [
			'session'   => $sessKey,
			'attrs'     => json_encode($attrs),
			'app'       => $app,
			'forward'   => $proxy['forward']
		];
		$res = self::callProxy($proxy['shared_key'], 'POST', 'https://'.$proxy['host'].'/hubpub/set-attributes', $qs);

		// not rcvd successfully
		if (!($res = json_decode($res, true)) || !isset($res['result']) || $res['result'] !== 'success') {
			if (isset($res['error'])) {
				error_log('hubpub proxy error: '.$res['error'].", context: ".self::logFriendlyCurlOpts($copts));
			}
			else {
				error_log(print_r($res, 1));
			}
			error_log('https://'.$proxy['host'].'/hubpub/set-attributes');
			throw new ServiceUnavailableError;
		}

		$query = isset($_SERVER['REDIRECT_QUERY_STRING']) ? $_SERVER['REDIRECT_QUERY_STRING'] : $_SERVER['QUERY_STRING'];
		$this->resp = ['text/html', self::view('app', [
			// @TODO
			'connect' => 'https://'.$proxy['host'].'/hubpub/session?path='.urlencode('/'.implode('/', $rest).($query ? '?'.$query : '')).'&session='.urlencode($sessKey),
			'session' => $sessKey,
			'title'   => $attrList['title']
		])];
	}

	private static function callProxy($key, $mtd, $url, $params = []) {
		$ch = curl_init();
		$copts = [
			\CURLOPT_URL            => $url,
			\CURLOPT_RETURNTRANSFER => true,
			\CURLOPT_HTTPHEADER     => [
				'X-HubPub-Shared-Key: '.$key
			]
		];
		if ($mtd === 'POST') {
			$copts[\CURLOPT_POST] = count($params);
			$copts[\CURLOPT_POSTFIELDS] = http_build_query($params);
		}
		curl_setopt_array($ch, $copts);
		$res = curl_exec($ch);
		if (($err = curl_error($ch))) {
			throw new ServiceUnavailableError($err);
		}
		curl_close($ch);
		return $res;
	}

	private static function logFriendlyCurlOpts($copts) {
		// rename numeric options. skip where null
		$logKeys = [
			\CURLOPT_URL            => 'url',
			\CURLOPT_POSTFIELDS     => 'post_fields',
			\CURLOPT_HTTPHEADER     => 'headers',
			\CURLOPT_POST           => NULL,
			\CURLOPT_RETURNTRANSFER => NULL
		];
		$logOpts = [];
		foreach ($copts as $k=>$v) {
			if (array_key_exists($k, $logKeys)) {
				if (!$logKeys[$k]) {
					continue;
				}
				$k = $logKeys[$k];
			}
			// not specifically named or skipped, show the numeric code
			else {
				$k = 'curlopt_code_'.$k;
			}

			// unpack post field url- and json-encoding
			if ($k == 'post_fields') {
				parse_str($v, $v);
				if ($v['attrs']) {
					$v['attrs'] = json_decode($v['attrs']);
				}
			}
			$logOpts[$k] = $v;
		}
		// rm shared key header if present
		$logOpts['headers'] = array_values(array_filter($logOpts['headers'], function($v) { return strpos($v, 'X-HubPub-Shared-Key') !== 0; }));
		return json_encode($logOpts);
	}

	public function __toString() {
		if ($this->resp[2]) {
			http_response_code($this->resp[2]);
		}
		header('Content-type: '.$this->resp[0]);
		return $this->resp[0] === 'application/json' && !is_string($this->resp[1]) ? json_encode($this->resp[1]) : $this->resp[1];
	}

	private function assertGroup($gid) {
		if (!$this->user->guest) {
			$sth = $this->dbh->prepare('SELECT 1 FROM #__xgroups_managers WHERE gidNumber = :gid AND uidNumber = :uid UNION SELECT 1 FROM #__xgroups_members WHERE gidNumber = :gid AND uidNumber = :uid');
			$sth->execute([':gid' => $gid, ':uid' => $this->user->id]);
			$res = $sth->fetchAll();
			if (!$res) {
				$sth = $this->dbh->prepare('SELECT description FROM #__xgroups WHERE gidNumber = :gid');
				$sth->execute([':gid' => $gid]);
				$row = $sth->fetch();
				throw new ForbiddenError($row ? 'Access is restricted to the '.$row['description'].' group' : 'Access is restricted to a group');
			}
		}
	}

	private function attributeRelease($key) {
		if ($this->user->guest) {
			return NULL;
		}
		switch ($key) {
			case 'username':
				return $this->user->username;
			case '':
				return null;
			case 'firstName':
				return $this->getProfile('givenName');
			case 'lastName':
				return $this->getProfile('surname');
			case 'email':
				return $this->user->get('email');
			default:
				throw new InternalServerError('Undefined attribute release: '.$key);
		}
	}

	private function getProfile($key = NULL) {
		if (!$this->profile) {
			$sth = $this->dbh->prepare('SELECT * FROM #__xprofiles WHERE uidNumber = :user_id');
			$sth->execute([':user_id' => $this->user->id]);
			$this->profile = $sth->fetch();
		}
		return $key ? $this->profile[$key] : $this->profile;
	}

	private static function view($name, $ctx = []) {
		extract($ctx);
		$h = function($str) { return htmlentities($str); };
		$a = function($str) { return str_replace('"', '&quot;', $str); };
		ob_start();
		require __DIR__.'/../views/'.$name.'.html.php';
		return ob_get_clean();
	}
}

try {
	if ((!isset($_SERVER['REDIRECT_HTTPS']) || $_SERVER['REDIRECT_HTTPS'] !== 'on') && (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on')) {
		\JFactory::getApplication()->redirect(str_replace('http://', 'https://', isset($_SERVER['REDIRECT_SCRIPT_URI']) ? $_SERVER['REDIRECT_SCRIPT_URI'] : $_SERVER['SCRIPT_URI']));
	}
	echo new Controller;
}
catch (HttpError $ex) {
	\JError::raiseError($ex->getHttpCode(), $ex->getMessage());
}
