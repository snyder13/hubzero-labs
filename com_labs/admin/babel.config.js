module.exports = {
	'presets': [
		[ '@babel/preset-env', {
			'targets': '> 0.25%, not dead'
		} ],
		'@babel/preset-react'
	],
	'plugins': [
		'babel-plugin-syntax-flow',
		'babel-plugin-transform-flow-strip-types',
		'@babel/plugin-transform-async-to-generator',
		'@babel/plugin-transform-arrow-functions',
		'@babel/plugin-proposal-object-rest-spread',
		'@babel/plugin-proposal-class-properties',
		'react-css-modules'
	]
};

