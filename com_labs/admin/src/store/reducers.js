import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable';

import labList from '../components/LabList/actions';

export const reducers = combineReducers({
	labList: labList.reducer
});

export const epics = combineEpics(
	...labList.epics
);
