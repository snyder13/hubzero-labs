import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, mergeMap, takeUntil, catchError } from 'rxjs/operators';
import { ofType } from 'redux-observable';

export const makeAjaxEpic = ({ type, url, method }) => (action$) => action$.ofType(type).pipe(
	mergeMap((action) => {
		const effUrl = typeof url === 'function' ? url(action) : url;
		const call = method === 'POST' ? ajax.post(effUrl, action.props, { 'Content-Type': 'application/json' }) : ajax.getJSON(effUrl);
		return call.pipe(
			map((payload) => ({ type: `${ type }_SUCCESS`, payload })),
			catchError((error) => of({ type: `${ type }_ERROR`, error })),
			takeUntil(action$.pipe(
				ofType(`${ type }_CANCELLED`)
			))
		 )
	})
);

