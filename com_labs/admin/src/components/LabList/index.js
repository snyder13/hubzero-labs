import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';

class LabItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			restore: props
		};
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if (!prevState.id) {
			return { ...prevState, ...nextProps };
		}
		return {
			...prevState,
			pristine: [ 'title', 'name', 'published', 'gcn', 'host', 'forward', 'shared_key', 'released_username', 'released_email', 'released_firstName', 'released_lastName' ]
				.every((field) => nextProps[field] === prevState[field])
		};
	}

	handleInputChange = ({ target }) => {
		const value = target.type === 'checkbox' ? target.checked : target.value;
		this.setState({
			[target.name]: value
		});
	}

	getInput(name, opts = {}) {
		const label = opts.label || name[0].toUpperCase() + name.slice(1).replace(/_/g, ' ');
		const inp = <input
			name={ name }
			value={ typeof(this.state[name]) === 'string' ? this.state[name] : '' }
			checked={ opts.type === 'checkbox' ? this.state[name] : null }
			onChange={ this.handleInputChange }
			type={ opts.type || 'text' }
			placeholder={ label }
		/>;
		const className = `inp-${ name }`
		return <div className={ [ 'inp-cont', className, opts.type || 'text' ].join(' ') }>
			<p className="inp">
				{ opts.type === 'checkbox' ? inp : '' }
				<label> { label } </label>
				{ opts.type === 'checkbox' ? '' : inp }
				{ opts.extra ? <span className="info">{ opts.extra }</span> : '' }
			</p>
		</div>;
	}

	restore = () => {
		this.setState({
			...this.state.restore
		});
	}

	render() {
		return <div>
			{ this.getInput('title') }
			{ this.getInput('name', { extra: 'Determines path, /labs/run/$name' }) }
			{ this.getInput('published', { type: 'checkbox' }) }
			{ this.getInput('forward', { label: 'Container URL' }) }
			{ this.getInput('host', { label: 'Proxy host' }) }
			{ this.getInput('shared_key', { label: 'Shared key', extra: 'Must match `shared-key` from `secrets.js` on the proxy host' }) }
			{ this.getInput('gcn', { label: 'Access restricted to group', extra: 'Optional, group id number or cn' }) }
			<div className="attrs">
				<p className="label">Released attributes</p>
				<ul>
					<li>{ this.getInput('released_username', { label: 'Username', type: 'checkbox' }) }</li>
					<li>{ this.getInput('released_email', { label: 'Email', type: 'checkbox' }) }</li>
					<li>{ this.getInput('released_firstName', { label: 'First name', type: 'checkbox' }) }</li>
					<li>{ this.getInput('released_lastName', { label: 'Last name', type: 'checkbox' }) }</li>
				</ul>
			</div>
			{ this.props.secret ? <div>
					<p>Session secret: <tt>{ this.props.secret }</tt></p>
					<p className="info">
						If the container is firewalled so that only the proxy can access its web services, you can ignore this. If not, this can be used to verify the legitimacy of the session headers: <br />
						<tt>sha1(x-hubpub-session | x-hubpub-nonce | session-secret) == x-hubpub-hash</tt>
					</p>
				</div>
				: <p><button disabled={ /^new-/.test(this.state.id) } onClick={ () => this.props.getSessionSecret(this.state.name) }>Show session secret</button></p>
			}
			<button
				disabled={ this.state.pristine }
				onClick={ () => this.props.onChange({ ...this.state, restore: undefined, onChange: undefined, pristine: undefined, published: !!this.state.published }) }
				>
				Save
			</button>
			<button
				disabled={ this.state.pristine }
				onClick={ this.restore }
				>
				Undo changes
			</button>
			{ this.props.error ? <p className="error">{ this.props.error }</p> : '' }
			{ this.props.confirm ? <p className="confirmation">{ this.props.confirm }</p> : '' }
		</div>;
	}
}

export class LabList extends React.Component {
	componentDidMount() {
		this.props.loadList();
	}

	render() {
		return <div>
			<h2>Labs</h2>
			<ol className="labs">
			{ this.props.items.map((item) =>
				<li key={ item.id }><LabItem { ...item } onChange={ this.props.saveLab } getSessionSecret={ this.props.getSessionSecret } /></li>
			) }
			</ol>
			<button onClick={ this.props.addEmptyLab }>Add</button>
		</div>;
	}
}

import { loadList, addEmptyLab, deleteLab, saveLab, getSessionSecret } from './actions';

export default connect(
	({ labList }) => ({ items: labList.items }),
	{ loadList, addEmptyLab, deleteLab, saveLab, getSessionSecret }
)(LabList);



