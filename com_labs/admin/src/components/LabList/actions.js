import { makeAjaxEpic } from '../../lib/ajax';

export const loadList = () => ({ type: 'LOAD_LABS' });
export const addEmptyLab = () => ({ type: 'ADD_EMPTY_LAB' });
export const deleteLab = (id) => ({ type: 'DELETE_LAB', id });
export const saveLab = (props) => ({ type: 'SAVE_LAB', props });
export const getSessionSecret = (lab) => ({ type: 'GET_SESSION_SECRET', lab });

const loadListEpic = makeAjaxEpic({ type: 'LOAD_LABS', url: '/labs/admin/labs' });
const saveLabEpic = makeAjaxEpic({ type: 'SAVE_LAB', url: '/labs/admin/labs', method: 'POST' });
const sessionSecretEpic = makeAjaxEpic({ type: 'GET_SESSION_SECRET', url: ({ lab }) => `/labs/admin/session-secret/${ lab }` });

const emptyItem = () => ({
	id: `new-${ Date.now() }`,
	published: true
});

const reducer = (state = { items: [] }, action) => {
	switch (action.type) {
		case 'LOAD_LABS_SUCCESS':
			return { items: action.payload.map((item) => ({ ...item, pristine: true })) };
		case 'ADD_EMPTY_LAB':
			return { items: [ ...state.items, emptyItem() ] }
		case 'SAVE_LAB':
			return { items: state.items.map((item) => ({ ...item, error: undefined })) };
		case 'GET_SESSION_SECRET_SUCCESS':
			return { items: state.items.map((item) => (item.name === action.payload.name ? { ...item, secret: action.payload.secret } : item)) };
		case 'SAVE_LAB_SUCCESS':
			const resp = action.payload.response;
			console.log(resp);
			return { items: state.items.map((item) => (item.id == resp.tempId ? { ...item, confirm: 'Saved', id: resp.id } : item)) };
		case 'SAVE_LAB_ERROR':
			const err = action.error.response;
			if (!err.ctx) {
				return state;
			}
			return { items: state.items.map((item) => ({ ...item, error: item.id == err.ctx.id ? err.error : undefined })) };
		default:
			console.log('weird action', action);
		return state;
	}
};

export default { reducer, epics: [ loadListEpic, saveLabEpic, sessionSecretEpic ] };
