const CHANGE_NAME = 'CHANGE_NAME';

export const changeName = (name) => ({
	type: CHANGE_NAME,
	payload: { name }
});

const reducers = {
	app: (state = {
		name: 'world'
	}, action) => {
		switch (action.type) {
			case CHANGE_NAME:
				return { ...state, name: action.payload.name };
			break;
			default: return state;
		}
	}
};
export default reducers;
