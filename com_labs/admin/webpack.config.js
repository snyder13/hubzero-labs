const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const dist = path.resolve('./dist')

module.exports = {
	'mode': 'none',
	'entry': [ './src' ],
	'devtool': 'source-map',
	'output': {
		'path': dist,
		'filename': 'bundle.js',
		'publicPath': '/'
	},
	'plugins': [
		new CopyWebpackPlugin([ {
			'from': 'assets/**',
			'context': path.resolve('./src'),
			'to': dist
		}, {
			'from': '*.html',
			'context': path.resolve('./src'),
			'to': dist
		}, {
			'from': 'node_modules/@babel/polyfill/dist/polyfill.min.js',
			'to': path.resolve(dist, 'vendor')
		}
		])
	],
	'module': {
		'rules': [
			{
				'test': /[.]js$/,
				'use': [ 'babel-loader' ],
				'exclude': /node_modules/
			},
			{
				'test': /\.p?css$/,
				'use': [
					{
						'loader': 'style-loader'
					}, {
						'loader': 'css-loader',
						'options': {
							'modules': true,
							'localIdentName': '[path]___[name]__[local]___[hash:base64:5]'
						}
					}, {
						'loader': 'postcss-loader'
					}
				]
			}
		]
	}
};
