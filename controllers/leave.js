'use strict';

const
	valid = require(`${__dirname}/../validations.js`),
	crypto = require('crypto')
	;

module.exports = (app, conf, log) => {
	app.get('/hubpub/leave', (req, res, next) => {
		if (!valid.hasQueryKeys(req, res, [ 'to', 'nonce', 'hash' ])) {
			return next;
		}
		app.pg.query('SELECT shared_key FROM labs WHERE name = $1', [ req.labName ])
			.then((dbRes) => {
				if (dbRes.rows.length == 0) {
					return next();
				}

				let sha1 = crypto.createHash('sha1');
				sha1.update(req.query.to, 'utf8');
				sha1.update(req.query.nonce, 'utf8');
				sha1.update(dbRes.rows[0].shared_key, 'utf8');
				if (req.query.hash !== sha1.digest('hex')) {
					res.status(403).render('errors/generic', { 'title': 'Forbidden', 'detail': 'A security exception was encountered with this link.' });
					return next();
				}

				res.render('leave', { 'to': req.query.to });
			}, (err) => {
				log.error('database error', { 'msg': err.toString() });
				res.status(500).render('errors/generic', { 'title': 'Database error' });
			});
	});
};
