'use strict';
const pendingAttributes = {}, valid = require(`${__dirname}/../validations.js`);

module.exports = (app, conf) => {
	// Take session attributes from the hub and hang on to them.
	// We expect the user to load the start-session path below with a key that traces back to this data.
	app.post('/hubpub/set-attributes', (req, res, next) => {
		// check whether processable and authorized
		if (!valid.hasBodyKeys(req, res, ['session', 'app', 'forward']) || !valid.assertInternal(req, res, conf)) {
			console.log('bad', valid.hasBodyKeys(req, res, [ 'session', 'app', 'forward' ]), req.body, req.headers);
			if (!res.headersSent) {
				next();
			}
			return;
		}
		// store attributes. not in req.session yet, because we're talking to the hub and not the client
		pendingAttributes[req.body.session] = req.body;
		// nonce from this request is checked again later when the client asks for the session, to make forgery more difficult
		pendingAttributes[req.body.session].nonce = req.headers['x-hubpub-nonce'];

		res.type('json').send({ 'result': 'success' });
	});

	// Match up a session key with something posted by the hub earlier and use it to initialize a client session.
	app.get('/hubpub/session', (req, res, next) => {
		/// @DEPRECATED
		if (req.query.session && !req.query.hash) {
		}
		else {
			// minimal data
			if (!valid.hasQueryKeys(req, res, ['session', 'nonce', 'hash', 'path'])) {
				console.log('missing keys');
				console.log(req.query);
				return;
			}
		}
		// must match prior submission to set-attributes
		if (!pendingAttributes[req.query.session]) {
			console.log('no pending session');
			res.status(422).render('errors/generic', { 'title': 'Unprocessable entity', 'detail': 'The request could not be completed because the supplied session key is not valid for this client.' });
			return;
		}

		/// @DEPRECATED
		if (req.query.hash) {
			// hash must match
			if (!valid.hash(req.query.hash, [
				req.query.session, // session id
				req.query.path,
				pendingAttributes[req.query.session].nonce, // nonce from set-attributes session, somewhat useful because the client should have no knowledge of it
				req.query.nonce, // current session nonce
				conf.get('shared-key')
			], res)) {
				console.log('hash failed');
				return next();
			}
		}
		// auxiliary attributes we don't need going forward
		const skip = {
			'nonce': true,
			'session': true
		};
		// bind to client session
		Object.keys(pendingAttributes[req.query.session]).forEach((key) => {
			if (skip[key]) {
				return;
			}
			req.session[key] = pendingAttributes[req.query.session][key];
		});
		req.session.forward = req.query.path;

		delete pendingAttributes[req.query.session];

		// set a CSP to block inline scripts...
		res.setHeader('content-security-policy', 'script-src \'self\' ');// + req.header('x-forwarded-host'));
		// .. and then test whether the browser executes it anyway, so we can warn people their client is bad
		res.render('start-session', { 'forward': req.query.path });
	});
}

