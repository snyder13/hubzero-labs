'use strict';
const valid = require(`${__dirname}/../validations.js`);
const uuid = require('uuid').v4;

module.exports = (app, conf) => {
	app.get('/hubpub/session-secret/:lab', async (req, res, next) => {
		if (!valid.assertInternal(req, res, conf)) {
			return;
		}
		const { rows } = await app.pg.query('SELECT shared_key AS secret FROM labs WHERE name = $1', [ req.params.lab ]);
		res.type('json').send(rows[0]);
	});

	app.post('/hubpub/rename/:from/:to', async (req, res, next) => {
		if (!valid.assertInternal(req, res, conf)) {
			return;
		}
		await app.pg.query('UPDATE labs SET name = $1 WHERE name = $2', [ req.params.to, req.params.from ]);
		res.type('json').send({ 'result': 'ok' });
	});

	app.post('/hubpub/update/:lab', async (req, res, next) => {
		const DEFAULT_TIMEOUT = 60;
		const DEFAULT_CSP = true;
		if (!valid.hasBodyKeys(req, res, ['forward', 'hub_host']) || !valid.assertInternal(req, res, conf)) {
			if (!res.headersSent) {
				next();
			}
			return;
		}

		const { rows: existing } = await app.pg.query('SELECT id FROM labs WHERE name = $1', [ req.params.lab ]);
		if (existing) {
			await app.pg.query('UPDATE labs SET forward = $1, hub_host = $2 WHERE name = $3', [ req.body.forward, req.body.hub_host, req.params.lab ]);
		}
		else {
			await app.pg.query('INSERT INTO labs(name, forward, hub_post, shared_key, timeout_seconds, csp_enabled) VALUES ($1, $2, $3, $4, $5, $6)', [
				req.params.name,
				req.body.forward,
				req.body.hub_host,
				uuid(),
				DEFAULT_TIMEOUT,
				DEFAULT_CSP
			]);
		}
		res.type('json').send({ 'result': 'ok' });
	});
}
