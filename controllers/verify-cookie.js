'use strict';

module.exports = (app, conf) => {
	app.get('/hubpub/verify-cookie', (req, res, next) => {
		if (req.session && req.session.forward) {
			res.redirect(req.session.forward);
			return;
		}
		res.status(400).render('errors/generic', { 'title': 'Bad request', 'detail': 'This application requires the user of browser cookies. Please ensure you have cookies enabled.' });
	});
}

