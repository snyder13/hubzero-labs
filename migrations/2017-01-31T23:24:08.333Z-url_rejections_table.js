'use strict';
module.exports = (dbh, conf, next, err) => {
	dbh.q.query(`CREATE TABLE url_rejections(
		id serial not null primary key,
		lab_id int not null references labs(id),
		pattern varchar(255) not null,
		redirect varchar(255) null
	)`)
		.then(next, err);
};
