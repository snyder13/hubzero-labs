'use strict';

const defaults = {
	'report-uri'     : '/hubpub/csp-report',
	'style-src'      : "'self' $HUB_HOST",
	'default-src'    : 'self',
	'font-src'       : "'self' $HUB_HOST",
	'object-src'     : "'none'",
	'frame-ancestors': '$HUB_HOST',
	'form-action'    : "'self'",
	'child-src'      : "'self'",
	'media-src'      : "'self'",
	'img-src'        : "'self' $HUB_HOST"
};

module.exports = (dbh, conf, next, err) => {
	dbh.q.query(`CREATE TABLE csp_directives(
		id serial not null primary key,
		lab_id int null references labs(id),
		key varchar(100) not null,
		value varchar(255) not null
	)`)
		.then(() => {
			const keys = Object.keys(defaults);
			let done = 0;

			const ins = () => {
				const key = keys[done];
				if (!key) {
					return next();
				}
				dbh.q.query({
					'text': 'INSERT INTO csp_directives(key, value) VALUES ($1, $2)',
					'name': 'ins_csp',
					'values': [ key, defaults[key] ]
				})
					.then(() => {
						++done;
						ins();
					}, err);
			};
			ins();
		});
};
