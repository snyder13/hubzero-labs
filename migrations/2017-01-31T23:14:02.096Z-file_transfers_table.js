'use strict';
module.exports = (dbh, conf, next, err) => {
	dbh.q.query(`CREATE TYPE transfer_direction AS ENUM ('upload', 'download')`)
		.then(() => {
			return dbh.q.query(`CREATE TABLE file_transfers(
				id serial not null primary key,
				lab_id int null references labs(id),
				pattern varchar(255) not null,
				direction transfer_direction not null
			)`);
		})
		.then(next, err);
};
