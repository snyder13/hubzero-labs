'use strict';
module.exports = (dbh, conf, next, err) => {
	dbh.q.query(`CREATE TABLE domains(
		id serial not null primary key,
		name varchar(100) not null,
		contact varchar(100) not null
	)`)
		.then(next, err);
};
