'use strict';
module.exports = (dbh, conf, next, err) => {
	dbh.q.query(`CREATE TABLE dom_rejections(
		id serial not null primary key,
		lab_id int not null references labs(id),
		selector varchar(255) not null
	)`)
		.then(next, err);
};
