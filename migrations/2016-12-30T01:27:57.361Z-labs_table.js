'use strict';
module.exports = (dbh, conf, next, err) => {
	dbh.q.query(`CREATE TABLE labs(
		id serial not null primary key,
		name varchar(50) not null,
		forward varchar(200) not null,
		shared_key varchar(50) not null,
		timeout_seconds int not null default 60,
		hub_host varchar(100) null
	)`)
		.then(next, err);
};
