'use strict';
module.exports = (dbh, conf, next, err) => {
	dbh.q.query(`CREATE TABLE allowed_links(
		id serial not null primary key,
		lab_id int null references labs(id),
		host varchar(255) not null
	)`)
		.then(next, err);
};
