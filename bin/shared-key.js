#!/usr/bin/env node
'use strict';

const fs = require('fs'), secretsFile = `${__dirname}/../secrets.json`;

let secrets = {}, generated = false;
if (fs.existsSync(secretsFile)) {
	secrets = JSON.parse(fs.readFileSync(secretsFile));
}
if (!secrets.base) {
	secrets.base = {};
}
if (!secrets.base['shared-key']) {
	secrets.base['shared-key'] = require('crypto').randomBytes(64).toString('base64');
	generated = true;
}
if (generated) {
	fs.writeFileSync(secretsFile, JSON.stringify(secrets, null, '\t'), { 'encoding': 'utf8' });
}
console.log(secrets.base['shared-key']);
