#!/bin/bash

PUB_ROOT="$(dirname $0)/../public/js/lib"
IFR_ROOT="$(dirname $0)/../node_modules/iframe-resizer/js"

mkdir -p "${PUB_ROOT}"

test ! -e "${PUB_ROOT}/js/lib/waves.min.js"     && ln -s "${IFR_DIST}/iframeResizer.contentWindow.js" "${PUB_ROOT}/iframeResizer.contentWindow.js"
test ! -e "${PUB_ROOT}/js/lib/waves.min.js.map" && ln -s "${IFR_DIST}/iframeResizer.contentWindow.min.js" "${PUB_ROOT}/iframeResizer.contentWindow.min.js"
test ! -e "${PUB_ROOT}/js/lib/waves.min.js"     && ln -s "${IFR_DIST}/iframeResizer.contentWindow.map" "${PUB_ROOT}/iframeResizer.contentWindow.map"

exit 0
