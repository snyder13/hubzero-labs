#!/usr/bin/env node
'use strict';
const fs = require('fs'), { execSync } = require('child_process'), thresholdDays = 30, timeDiff = thresholdDays * 24 * 60 * 60 * 1000, now = (new Date).getTime();
if (fs.readdirSync('/etc/letsencrypt/live').some((dir) => {
	if (!/[.]./.test(dir)) {
		return false;
	}
	try {
		const expires = execSync(`echo | openssl s_client -connect ${dir}:443 2> /dev/null | openssl x509 -noout -enddate`, { 'encoding': 'utf8' });
		if ((new Date(expires.replace(/^\s+|\s*notAfter=\s*|\s+$/g, ''))).getTime() - now < timeDiff) {
			return true;
		}
		return false;
	}
	catch (ex) {
		console.error(ex);
		return false;
	}
})) {
	const server = execSync(`ls /etc/init.d/ |grep -E '(httpd|apache2?)$'`, { 'encoding': 'utf8' }).replace(/\s+$/, '');
	execSync(`/etc/init.d/${server} stop`);
	console.log(execSync(`/usr/bin/certbot renew`, { 'encoding': 'utf8' }));
	execSync(`/etc/init.d/${server} start`);
}
