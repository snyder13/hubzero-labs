'use strict';
module.exports = {
	'set-lab': {
		'deps': [ 'hex.pg', 'hex.base', 'hex.redis' ]
	},
	'reject-urls': {
		'deps': [ 'set-lab' ]
	},
	'fix-request-headers': {
		'deps': [ 'reject-urls', 'hex.session' ]
	},
	'lab-forward': {
		'deps': [ 'fix-request-headers', 'hex.session', 'hex.body-parser' ]
	},
	'fix-response-headers': {
		'deps': [ 'lab-forward' ]
	},
	'string-replace-host': {
		'deps': [ 'lab-forward' ]
	},
	'parse-dom': {
		'deps': [ 'string-replace-host' ]
	},
	'inject-common-fixup': {
		'deps': [ 'parse-dom' ]
	},
	'filter-links': {
		'deps': [ 'parse-dom' ]
	},
	'reject-dom': {
		'deps': [ 'parse-dom', 'hex.session' ]
	},
	'csp-directives': {
		'deps': [ 'parse-dom' ]
	},
	'filter-attachments': {
		'deps': [ 'lab-forward' ]
	},
	'stringify-dom': {
		'deps': [ 'inject-common-fixup', 'filter-links', 'reject-dom', 'filter-attachments', 'fix-response-headers', 'csp-directives' ]
	},
	'controllers': {
		'deps': [ 'hex.access-log', 'hex.pg', 'hex.session' ]
	}
};
